
#ifndef G729C_G729_H
#define G729C_G729_H

#include <stddef.h>
#include <stdint.h>

#define  L_FRAME                80
#define  L_FRAME_COMPRESSED     10

size_t g729_encoder_size();
void g729_encoder_init(void *encoder);
void g729_encoder_proc(void *encoder, const int16_t *input_pcm, uint8_t *output_bitstream);
void g729_encoder_deinit(void *encoder);

size_t g729_decoder_size();
void g729_decoder_init(void *decoder);
void g729_decoder_proc(void *decoder, const uint8_t *input_bitstream, int bfi, int16_t *output_pcm);
void g729_decoder_deinit(void *decoder);


#endif // G729C_G729_H