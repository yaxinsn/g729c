
#ifndef G729C_G729A_H
#define G729C_G729A_H

#include <stdint.h>
#include "ld8a.h"

struct g729a_encoder {
    struct pre_proc pre_proc;
    struct cod_ld8a ld8;
};

struct g729a_decoder {
    struct dec_ld8a ld8;
    struct postfilter postfilter;
    struct postprocess postprocess;

    FLOAT synth_buf[L_FRAME + M]; /* Synthesis */
    FLOAT *synth;
};

void g729a_encoder_init(struct g729a_encoder *encoder);
void g729a_encoder_proc(struct g729a_encoder *encoder, const int16_t *input_pcm, uint8_t *output_bitstream);
void g729a_encoder_deinit(struct g729a_encoder *encoder);

void g729a_decoder_init(struct g729a_decoder *decoder);
void g729a_decoder_proc(struct g729a_decoder *decoder, const uint8_t *input_bitstream, int bfi, int16_t *output_pcm);
void g729a_decoder_deinit(struct g729a_decoder *decoder);

#endif // G729C_G729A_H