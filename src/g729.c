
#include "g729.h"
#include "version.h"
#ifdef VER_G729A
#include "g729a.h"
typedef struct g729a_encoder encoder_t;
typedef struct g729a_decoder decoder_t;
#define encoder_init g729a_encoder_init
#define encoder_proc g729a_encoder_proc
#define encoder_deinit g729a_encoder_deinit
#define decoder_init g729a_decoder_init
#define decoder_proc g729a_decoder_proc
#define decoder_deinit g729a_decoder_deinit
#else
#include "g729k.h"
typedef struct g729k_encoder encoder_t;
typedef struct g729k_decoder decoder_t;
#define encoder_init g729k_encoder_init
#define encoder_proc g729k_encoder_proc
#define encoder_deinit g729k_encoder_deinit
#define decoder_init g729k_decoder_init
#define decoder_proc g729k_decoder_proc
#define decoder_deinit g729k_decoder_deinit
#endif

size_t g729_encoder_size()
{
    return sizeof(encoder_t);
}
void g729_encoder_init(void *encoder)
{
    encoder_init(encoder);
}
void g729_encoder_proc(void *encoder, const int16_t *input_pcm, uint8_t *output_bitstream)
{
    encoder_proc(encoder, input_pcm, output_bitstream);
}
void g729_encoder_deinit(void *encoder)
{
    encoder_deinit(encoder);
}

size_t g729_decoder_size()
{
    return sizeof(decoder_t);
}
void g729_decoder_init(void *decoder)
{
    decoder_init(decoder);
}
void g729_decoder_proc(void *decoder, const uint8_t *input_bitstream, int bfi, int16_t *output_pcm)
{
    decoder_proc(decoder, input_bitstream, bfi, output_pcm);
}
void g729_decoder_deinit(void *decoder)
{
    decoder_deinit(decoder);
}
