
#ifndef G729C_G729K_H
#define G729C_G729K_H

#include <stdint.h>
#include "ld8k.h"

struct g729k_encoder {
    struct pre_proc pre_proc;
    struct cod_ld8k ld8;
};

struct g729k_decoder {
    struct dec_ld8k ld8;
    struct postfilter postfilter;
    struct postprocess postprocess;

    FLOAT synth_buf[L_FRAME+M]; /* Synthesis */
    FLOAT *synth;

    int voicing; /* voicing for previous subframe */
};

void g729k_encoder_init(struct g729k_encoder *encoder);
void g729k_encoder_proc(struct g729k_encoder *encoder, const int16_t *input_pcm, uint8_t *output_bitstream);
void g729k_encoder_deinit(struct g729k_encoder *encoder);

void g729k_decoder_init(struct g729k_decoder *decoder);
void g729k_decoder_proc(struct g729k_decoder *decoder, const uint8_t *input_bitstream, int bfi, int16_t *output_pcm);
void g729k_decoder_deinit(struct g729k_decoder *decoder);

#endif // G729C_G729K_H